var car = new Car("WEA123", new Account("Néstor García", "INE"));
car.passenger = 4;
car.printDataCar();

var uberX = new UberX("ABC123", new Account("Andrea Torres", "INE234"), "Chevrolet", "Spark");
uberX.passenger = 4;
uberX.printDataCar();

console.log("!")

/*

var userNestor = new User("Néstor García", "INEGAON");
userNestor.email = "ngarcia@gmail.com";
userNestor.password = "PASS";
userNestor.printDataAccount();
*/