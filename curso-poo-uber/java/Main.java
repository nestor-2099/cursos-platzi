class Main {
  public static void main(String[] args) {
    System.out.println("Hola mundo");
    UberX uberX = new UberX("AMQ123", new Account("Néstor García", "INE"), "Chevrolet", "Sonic");
    uberX.setPassenger(4);
    uberX.printDataCar();

    UberVan uberVan = new UberVan("DMC123", new Account("Andres", "INE345"));
    uberVan.setPassenger(6);
    uberVan.printDataCar();
    
    /*
    Car car2 = new Car("OPQ889", new Account("Andrea", "INE 2"));
    car2.passenger = 6;
    car2.printDataCar();
    */
  }
}