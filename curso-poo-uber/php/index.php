<?php
require_once("car.php");
require_once("uberX.php");
require_once("uberPool.php");
require_once("uberVan.php");
require_once("account.php");

$uberX = new UberX("AMZ123", new Account("Néstor García", "INE"), "Chevrolet", "Spark");
$uberX->setPassenger(4);
$uberX->printDataCar();
echo "<br>";

$uberPool = new UberPool("AMZ993", new Account("Brenda Castillo", "INE"), "Chevrolet", "Spark");
$uberX->setPassenger(4);
$uberPool->printDataCar();
echo "<br>";

$uberVan = new UberVan("OJL395", new Account("Raúl Ramírez", "AND456"), "Nissan", "Versa");
$uberVan->setPassenger(6);
$uberVan->printDataCar();


?>